import React from 'react'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import configureStore from './helpers/store'
import App from './App'
import NavBar from './components/NavBar'
import Employees from './Employees/Employees'
import NotFound from './NotFound'
import './index.css'

const store = configureStore()

// const openScreen = (props, navigateTo) => {
//   if (navigateTo && props.location.pathname !== `/${navigateTo}`) {
//     props.history.replace(`/${navigateTo}`)
//     return
//   }

//   props.history.replace('/')
// }
// const redirectWhenAuthenticated = component =>
//   redirectWhenAuthenticatedHOC(component, props => {
//     const { params } = props.match
//     const { redirectUrl } = params

//     openScreen(props, redirectUrl)
//   })
// const requireLogin = component => {
//   if (process.env.REACT_APP_DISABLE_AUTHENTICATION === 'true') {
//     return component
//   }
//   return requireLoginHOC(
//     component,
//     props => {
//       openScreen(props, 'auth')
//     },
//     null,
//     true
//   )
// }

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter onUpdate={() => window.scrollTo(0, 0)}>
      <App>
        <NavBar />
        <Switch>
          {/* <Route
            path="/auth/:redirectUrl?"
            component={redirectWhenAuthenticated(Login)}
          /> */}
          {/* <Route path="/logout" exact component={Logout} /> */}
          <Route path="/" exact component={App} />
          <Route path="/employees" component={Employees} />
          <Route component={NotFound} /> {/*404*/}
        </Switch>
      </App>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
)
