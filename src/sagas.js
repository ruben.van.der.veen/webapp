import { call, all } from 'redux-saga/effects'

import employees from './Employees/saga'

export default function* rootSaga() {
  yield all([call(employees)])
}
