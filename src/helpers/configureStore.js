import { createStore, compose, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose

const initialState = {}

export default function configureStore(rootReducer, mySaga) {
  // create the saga middleware
  const sagaMiddleware = createSagaMiddleware()

  // mount it on the Store
  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleware))
  )

  // then run the saga
  sagaMiddleware.run(mySaga)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = rootReducer.default
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}
