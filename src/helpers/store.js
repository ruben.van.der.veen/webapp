import configureStore from './configureStore'
import rootReducer from '../reducers'
import mySaga from '../sagas'

export default () => configureStore(rootReducer, mySaga)
