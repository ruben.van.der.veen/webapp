import React, { Component } from 'react'
import { connect } from 'react-redux'
import { loadList, load } from './actions'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

class Employees extends Component {
  componentDidMount() {
    this.props.loadList()
  }
  render() {
    console.log(this.props.list)
    return (
      <Paper
        style={{
          width: '100%',
          marginTop: 10,
          overflowX: 'auto',
        }}
      >
        <Table
          style={{
            minWidth: 650,
          }}
        >
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>City</TableCell>
              <TableCell>Age</TableCell>
              <TableCell>Status</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.props.list &&
              this.props.list.map(row => (
                <TableRow key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell>{row.city}</TableCell>
                  <TableCell>{row.age}</TableCell>
                  <TableCell>{row.status ? 'Active' : 'Inactive'}</TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </Paper>
    )
  }
}

const mapStateToProps = state => ({
  ...state.employees,
})
export default connect(
  mapStateToProps,
  { loadList, load }
)(Employees)
