import * as a from './actions'

const initialState = {
  data: {},
  list: [],
  loaded: false,
  error: false,
  loading: false,
}
const employees = (state = initialState, { type, payload, error, meta }) => {
  switch (type) {
    case a.LOAD_LIST: {
      return {
        ...state,
        loading: true,
        loaded: false,
      }
    }

    case a.LOADED_LIST: {
      return error
        ? {
            ...state,
            error: true,
            loading: false,
            loaded: true,
          }
        : {
            ...state,
            list: payload,
            error: false,
            loading: false,
            loaded: true,
          }
    }
    case a.LOAD: {
      return {
        ...state,
        loading: true,
        loaded: false,
      }
    }

    case a.LOADED: {
      return error
        ? {
            ...state,
            error: true,
            loading: false,
            loaded: true,
          }
        : {
            ...state,
            data: payload,
            error: false,
            loading: false,
            loaded: true,
          }
    }
    default:
      return state
  }
}

export default employees
