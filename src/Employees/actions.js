import { actionCreator as ac } from '../helpers/actionCreator'

const ns = type => `EMPLOYEES_${type}`

export const LOAD_LIST = ns('LOAD_LIST')
export const loadList = ac(LOAD_LIST)

export const LOADED_LIST = ns('LOADED_LIST')
export const loadedList = ac(LOADED_LIST)

export const LOAD = ns('LOAD')
export const load = ac(LOAD)

export const LOADED = ns('LOADED')
export const loaded = ac(LOADED)
