import { call, put, takeLatest } from 'redux-saga/effects'
import api from 'react-api'
import * as a from './actions'

function* loadList() {
  try {
    let response = yield call(api, {
      method: 'GET',
      path: `employees`,
    })
    console.log('Loaded list: ', response)
    yield put(a.loadedList(response))
  } catch (e) {
    console.log(e)
    yield put(a.loadedList(null, null, true))
  }
}

function* load(params) {
  try {
    const { id } = params
    let response = yield call(api, {
      method: 'GET',
      path: `employees/${id}`,
    })
    yield put(a.loaded(response))
  } catch (e) {
    console.log(e)
    yield put(a.loaded(null, null, true))
  }
}

export default function* mySaga() {
  yield takeLatest(a.LOAD_LIST, loadList)
  yield takeLatest(a.LOAD, load)
}
