import { combineReducers } from 'redux'

import employees from './Employees/reducer'

export default combineReducers({
  employees,
})
